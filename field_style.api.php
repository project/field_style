<?php

/**
 * @file
 */

/**
 * Hook_field_style_global_style().
 *
 * This hook is used to provide a few global style pre-configured. On the properties, you need to enable de 'globalStyle'.
 *
 * $vars = [
 *    [
 *      'value' => 'global_style_id_1',
 *      'label' => 'Roboto 20px white underlined',
 *      'style' => [
 *        "Default" => [
 *          "default" => [
 *            "fontFamily" => "Roboto",
 *            "fontWeight" => "100",
 *            "fontSize" => "22",
 *            "color" => "red",
 *          ],
 *          "hover" => [
 *            'color' => 'white'
 *          ]
 *        ],
 *        "Mobile" => [
 *          "active" => [
 *            "fontFamily" => "Roboto",
 *            "fontWeight" => "500"
 *          ]
 *        ]
 *      ]
 *    ]
 *  ];.
 */
function hook_field_style_global_style(&$values) {
}

/**
 * Hook_field_style_break_points().
 *
 * This hook is called before render style editor.
 *
 * [
 *  "Default" => [
 *    "icon" => "http://localhost:9090/sites/default/files/field_style/devices.svg"
 *    "weight" => "0"
 *    "name" => "Default"
 *    "media_queries" => ""
 *  ]
 *  "Tablet" => [
 *    "icon" => "http://localhost:9090/sites/default/files/field_style/tablet.svg"
 *    "name" => "Tablet"
 *    "media_queries" => "@media (min-width: 993px)"
 *    "weight" => "0"
 *  ]
 *  "Mobile" => [
 *    "icon" => "http://localhost:9090/sites/default/files/field_style/smartphone.svg"
 *    "name" => "Mobile"
 *    "media_queries" => "@media (min-width: 768px)"
 *    "weight" => "0"
 *  ]
 * ]
 */
function hook_field_style_break_points(&$values) {
}

/**
 * Hook_field_style_google_fonts().
 * [
 *    0 => [
 *      "family" => "Akaya Kanadaka",
 *      "variants" => [
 *          '100',
 *          '100italic',
 *      ],
 *    ],
 *    1 => [
 *      "family" => "Roboto",
 *      "variants" => [
 *          '100',
 *          '100italic',
 *       ], 
 *      ],
 *    ],
 *  ].
 */
function hook_field_style_google_fonts(&$values) {
}

/**
 * Hook_field_style_custom_font().
 * [
 *    "Akaya Kanadaka" => [
 *      "family" => "Akaya Kanadaka",
 *      "variants" => [
 *          '100',
 *          '100italic',
 *      ],
 *      "files" => [
 *          "100" => [
 *              'ttf' => 'path/to/akaya.ttf',
 *          ],
 *          "100italic" => [
 *              'eot' => 'path/to/akaya.eot',
 *         ],
 *      ],
 *    ],
 *    "Roboto" => [
 *      "family" => "Roboto",
 *      "variants" => [
 *          '100',
 *          '100italic',
 *       ],
 *       "files" => [
 *          "100" => [
 *              'ttf' => 'path/to/roboto.ttf',
 *          ],
 *          "100italic" => [
 *              'eot' => 'path/to/roboto.eot',
 *         ],
 *      ],
 *    ],
 *  ].
 */
function hook_field_style_custom_font(&$values) {
}

/**
 * Hook_field_style_global_image_style().
 *
 * This hook is called to provide the image styles for background image property.
 *
 * [
 *     "max_650x650" => Drupal\image\Entity\ImageStyle,
 *     "large" => Drupal\image\Entity\ImageStyle,
 *     "max_1300x1300" => Drupal\image\Entity\ImageStyle,
 *     "max_2600x2600" => Drupal\image\Entity\ImageStyle,
 *     "max_325x325" => Drupal\image\Entity\ImageStyle,
 *     "medium" => Drupal\image\Entity\ImageStyle,
 *     "thumbnail" => Drupal\image\Entity\ImageStyle,
 *   ]
 */
function hook_field_style_global_image_style(&$values) {
}

/**
 * Hook_field_style_property_background_image().
 *
 * This hook is called before to generate the background image property value.
 *
 * [
 *  "#value" => [
 *    "image" =>  83,
 *    "image_style" => "large"
 *  ]
 * ]
 *
 * @return
 *   [
 *   "#value" => [
 *    "image" =>  83,
 *    "image_style" => "large"
 *   ],
 *   "value" => "url('path/to/image')"
 *   ]
 */
function hook_field_style_property_background_image(&$values) {
}

/**
 * Hook_field_style_property().
 *
 * This hook is called before to generate the property value.
 *
 * [
 *  "#value" => "180px"
 * ]
 *
 * @return
 *   [
 *   "#value" => "180px"
 *   "value" => "180px"
 *   ]
 */
function hook_field_style_property(&$values) {
}


/**
 * Hook_field_style_property_PROPERTY().
 *
 * This hook is called before generating the specific property value.
 *
 * [
 *  "#value" => "180px"
 * ]
 *
 * @return
 *   [
 *   "#value" => "180px"
 *   "value" => "180px"
 *   ]
 */
function hook_field_style_property_PROPERTY(&$values) {
}

/**
 * Hook_field_style_global_pre_render_style().
 *
 * This hook is called before render the css style and put on head of the page.
 *
 * {
 *   "globalProperty": {
 *       "global_style": "global_style_id_1"
 *       "images": array:2
 *       0 => {
 *           "id": 83
 *           "url": "/sites/default/files/field-style/attachments/pexels-photo-263041.jpeg"
 *       }
 *       1 => {
 *           "id": 88
 *           "url": "/sites/default/files/field-style/attachments/pexels-photo-302899.jpeg"
 *       }
 *     ]
 *   }
 *   "Default": {
 *       "default": {
 *       "color": ""
 *       "backgroundColor": ""
 *   }
 *   "Tablet": {
 *       "default": {
 *       "backgroundColor": "red"
 *       }
 *   }
 * }
 */
function hook_field_style_global_pre_render_style(&$values) {
}
