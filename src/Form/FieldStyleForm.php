<?php

namespace Drupal\field_style\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Database\Connection;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class FieldStyleForm.
 *
 * @package Drupal\field_style\Form
 */
class FieldStyleForm extends ConfigFormBase {
  protected $database;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container
      ->get('database'));
  }

  /**
   * Construct a form.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection.
   */
  public function __construct(Connection $database) {
    $this->database = $database;
    $dir = 'public://field-style/settings';
    \Drupal::service('file_system')->prepareDirectory($dir, 1);

  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'field_style.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'field_style_form';
  }

  /**
   * {@inheritdoc}
   */

  /**
   * Form with 'add more' and 'remove' buttons.
   *
   * This example shows a button to "add more" - add another textfield, and
   * the corresponding "remove" button.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $config = $this->config('field_style.settings');

    $values = $config->get('settings');

    $google_font_family_key = $config->get('google_font_family_key');
    $google_font_family = $config->get('google_font_family');

    $break_points = $config->get('break_points');

    if (!empty($break_points)) {
      $has_default = FALSE;
      foreach ($break_points as $key => $value) {
        if ($value['name'] == 'Default') {
          $has_default = TRUE;
        }
      }
      if (!$has_default) {
        $break_points[0] = [
          'weight' => 0,
          'icon' => [],
          'name' => 'Default',
          'min' => '',
          'max' => '',
        ];
      }
    }
    else {
      $break_points[0] = [
        'weight' => 0,
        'icon' => [],
        'name' => 'Default',
        'min' => '',
        'max' => '',
      ];
    }

    // $paragraphs = $config->get('paragraphs');
    $num_google_font_family = $form_state->get('num_google_font_family');
    if ($num_google_font_family === NULL) {
      $num_google_font_family = $google_font_family && $google_font_family ? count($google_font_family) : 1;
      $form_state->set('num_google_font_family', $num_google_font_family);
    }

    $num_google_font_family = $form_state->get('num_google_font_family');

    $form['#tree'] = TRUE;

    $form['#attached'] = [
      'library' => [
        'field_style/admin',
      ],
    ];

    $form['google_font_family'] = [
      '#type' => 'details',
      '#title' => $this->t('Google Fonts'),
      '#prefix' => '<div id="google-font-family-wrapper">',
      '#suffix' => '</div>',
      '#open' => TRUE,
    ];

    $form['google_font_family']['google_font_family_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Google API KEY'),
      '#default_value' => $google_font_family_key ? $google_font_family_key : '',
    ];

    $form['google_font_family']['table'] = [
      '#type' => 'table',
      '#header' => [
        $this
          ->t('Font Family'),
        $this
          ->t('Weight'),
      ],
      '#empty' => $this
        ->t(($google_font_family_key == '' ? 'Sorry, There isn\'t google api key' : 'Sorry, There are no items!')),
      '#tabledrag' => [
        [
          'action' => 'order',
          'relationship' => 'sibling',
          'group' => 'table-sort-weight',
        ],
      ],
      '#tree' => TRUE,
      '#open' => TRUE,
    ];

    if ($google_font_family_key) {
      for ($i = 0; $i < $num_google_font_family; $i++) {
        $form['google_font_family']['table'][$i]['#attributes']['class'][] = 'draggable';
        $form['google_font_family']['table'][$i]['#weight'] = $values['google_font_family']['table'][$i]['weight'] ?? 0;

        $form['google_font_family']['table'][$i]['name'] = [
          '#type' => 'textfield',
          '#title' => $this->t('Name'),
          '#autocomplete_route_name' => 'field_style.autocomplete',
          '#autocomplete_route_parameters' => ['field_name' => 'name', 'count' => 10],
          '#default_value' => isset($google_font_family[$i]['name']) && $google_font_family[$i]['name'] ? $google_font_family[$i]['name'] : '',
        ];
        $form['google_font_family']['table'][$i]['weight'] = [
          '#type' => 'weight',
          '#title' => $this->t('Weight for @title', ['@title' => isset($google_font_family[$i]['name']) ? $google_font_family[$i]['name'] : '']),
          '#title_display' => 'invisible',
          '#default_value' => isset($google_font_family[$i]['weight']) && $google_font_family[$i]['weight'] ?? 0,
          '#attributes' => [
            'class' => [
              'table-sort-weight',
            ],
          ],
        ];
      }

      $form['google_font_family']['actions'] = [
        '#type' => 'actions',
      ];

      $form['google_font_family']['actions']['add_name'] = [
        '#type' => 'submit',
        '#value' => $this->t('Add Font'),
        '#submit' => ['::addOneGoogleFontFamily'],
        '#ajax' => [
          'callback' => '::addmoreGoogleFontFamilyCallback',
          'wrapper' => 'google-font-family-wrapper',
        ],
      ];

      if ($num_google_font_family > 1) {
        $form['google_font_family']['actions']['remove_name'] = [
          '#type' => 'submit',
          '#value' => $this->t('Remove Font'),
          '#submit' => ['::removeGoogleFontFamilyCallback'],
          '#ajax' => [
            'callback' => '::addmoreGoogleFontFamilyCallback',
            'wrapper' => 'google-font-family-wrapper',
          ],
        ];
      }
    }

    $num_break_points = $form_state->get('num_break_points');
    if ($num_break_points === NULL) {

      $num_break_points = [[
        'weight' => 0,
        'icon' => [],
        'name' => 'Default',
        'min' => '',
        'max' => '',
      ],
      ];

      $num_break_points = isset($break_points) ? count($break_points) : 1;
      $form_state->set('num_break_points', $num_break_points);
    }

    $num_break_points = $form_state->get('num_break_points');

    $form['#tree'] = TRUE;

    $form['break_points'] = [
      '#type' => 'details',
      '#title' => $this->t('Breakpoints'),
      '#prefix' => '<div id="break-points-wrapper">',
      '#suffix' => '</div>',
      '#open' => TRUE,
      '#description' => $this->t('<b>@media</b>: Fill in the @media rules. Example: <b>@media screen and (max-width: 900px) and (min-width: 600px), (min-width: 1100px) </b>'),
    ];

    $form['break_points']['table'] = [
      '#type' => 'table',
      '#header' => [
        $this
          ->t('Icon'),
        $this
          ->t('Name'),
        $this
          ->t('@media'),
        $this
          ->t('Weight'),
      ],
      '#empty' => $this
        ->t('Sorry, There are no items!'),
      '#tabledrag' => [
        [
          'action' => 'order',
          'relationship' => 'sibling',
          'group' => 'table-sort-weight',
        ],
      ],
      '#tree' => TRUE,
      '#open' => TRUE,
    ];
    $form['break_points']['table']['#attributes']['class'][] = 'table-breakpoints';

    for ($i = 0; $i < $num_break_points; $i++) {
      $form['break_points']['table'][$i]['#attributes']['class'][] = 'draggable';
      $form['break_points']['table'][$i]['#weight'] = $break_points[$i]['weight'] ?? 0;
      $form['break_points']['table'][$i]['icon'] = [
        '#type' => 'managed_file',
        '#title' => t('Profile Picture'),
        '#upload_validators' => [
          'file_validate_extensions' => ['gif png jpg jpeg svg'],
          'file_validate_size' => [25600000],
        ],
        '#theme' => 'image_widget',
        '#preview_image_style' => 'medium',
        '#upload_location' => 'public://field-style/settings',
        '#default_value' => $break_points[$i]['icon'] ?? '',
      ];

      $form['break_points']['table'][$i]['name'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Name'),
        '#size' => 100,
        '#disabled' => isset($break_points[$i]['name']) && $break_points[$i]['name'] == 'Default',
        '#default_value' => $break_points[$i]['name'] ?? '',
      ];

      $form['break_points']['table'][$i]['media_queries'] = [
        '#type' => 'textfield',
        '#title' => $this->t('@media'),
        '#size' => 100,
        '#disabled' => isset($break_points[$i]['name']) && $break_points[$i]['name'] == 'Default',
        '#default_value' => $break_points[$i]['media_queries'] ?? '',
      ];

      $form['break_points']['table'][$i]['weight'] = [
        '#type' => 'weight',
        '#title' => $this->t('Weight for @title', ['@title' => isset($break_points[$i]['name']) ? $break_points[$i]['name'] : '']),
        '#title_display' => 'invisible',
        '#default_value' => $break_points['table'][$i]['weight'] ?? 0,
        '#attributes' => [
          'class' => [
            'table-sort-weight',
          ],
        ],
      ];
    }

    $form['break_points']['actions'] = [
      '#type' => 'actions',
    ];

    $form['break_points']['actions']['add_name'] = [
      '#type' => 'submit',
      '#value' => $this->t('Add Break Point'),
      '#submit' => ['::addOne'],
      '#ajax' => [
        'callback' => '::addmoreCallback',
        'wrapper' => 'break-points-wrapper',
      ],
    ];

    if ($num_break_points > 1) {
      $form['break_points']['actions']['remove_name'] = [
        '#type' => 'submit',
        '#value' => $this->t('Remove Break Point'),
        '#submit' => ['::removeCallback'],
        '#ajax' => [
          'callback' => '::addmoreCallback',
          'wrapper' => 'break-points-wrapper',
        ],
      ];
    }

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
    ];

    return $form;
  }

  /**
   *
   */
  private function _getFields($entity) {
    $fields = [];
    foreach ($entity as $key => $value) {
      if (strpos($key, 'field_') === 0) {
        $fields[$key] = $value;
      }
    }
    return $fields;
  }

  /**
   * Callback for both ajax-enabled buttons.
   *
   * Selects and returns the fieldset with the names in it.
   */
  public function addmoreCallback(array &$form, FormStateInterface $form_state) {
    $name_field = $form_state->get('num_break_points');
    $add_button = $name_field + 1;
    $form_state->set('num_break_points', $add_button);
    return $form['break_points'];
  }

  /**
   * Submit handler for the "add-one-more" button.
   *
   * Increments the max counter and causes a rebuild.
   */
  public function addOne(array &$form, FormStateInterface $form_state) {
    $name_field = $form_state->get('num_break_points');
    $add_button = $name_field + 1;
    $form_state->set('num_break_points', $add_button);
    // Since our buildForm() method relies on the value of 'num_break_points' to
    // generate 'name' form elements, we have to tell the form to rebuild. If we
    // don't do this, the form builder will not call buildForm().
    $form_state->setRebuild();
  }

  /**
   * Submit handler for the "remove one" button.
   *
   * Decrements the max counter and causes a form rebuild.
   */
  public function removeCallback(array &$form, FormStateInterface $form_state) {
    $name_field = $form_state->get('num_break_points');
    if ($name_field > 1) {
      $remove_button = $name_field - 1;
      $form_state->set('num_break_points', $remove_button);
    }
    // Since our buildForm() method relies on the value of 'num_break_points' to
    // generate 'name' form elements, we have to tell the form to rebuild. If we
    // don't do this, the form builder will not call buildForm().
    $form_state->setRebuild();
  }

  /**
   * Submit handler for the "add-one-more" button.
   *
   * Increments the max counter and causes a rebuild.
   */
  public function addOneGoogleFontFamily(array &$form, FormStateInterface $form_state) {
    $name_field = $form_state->get('num_google_font_family');
    $add_button = $name_field + 1;
    $form_state->set('num_google_font_family', $add_button);
    // Since our buildForm() method relies on the value of 'num_google_font_family' to
    // generate 'name' form elements, we have to tell the form to rebuild. If we
    // don't do this, the form builder will not call buildForm().
    $form_state->setRebuild();
  }

  /**
   * Submit handler for the "remove one" button.
   *
   * Decrements the max counter and causes a form rebuild.
   */
  public function removeGoogleFontFamilyCallback(array &$form, FormStateInterface $form_state) {
    $name_field = $form_state->get('num_google_font_family');
    if ($name_field > 1) {
      $remove_button = $name_field - 1;
      $form_state->set('num_google_font_family', $remove_button);
    }
    // Since our buildForm() method relies on the value of 'num_google_font_family' to
    // generate 'name' form elements, we have to tell the form to rebuild. If we
    // don't do this, the form builder will not call buildForm().
    $form_state->setRebuild();
  }

  /**
   * Callback for both ajax-enabled buttons.
   *
   * Selects and returns the fieldset with the names in it.
   */
  public function addmoreGoogleFontFamilyCallback(array &$form, FormStateInterface $form_state) {
    $name_field = $form_state->get('num_google_font_family');
    $add_button = $name_field + 1;
    $form_state->set('num_google_font_family', $add_button);
    return $form['google_font_family'];
  }

  /**
   * Final submit handler.
   *
   * Reports what values were finally set.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // $values = $form_state->getValue(['break_points', 'name']);
    $output = $this->t('Configs are been saved!');
    $this->messenger()->addMessage($output);

    $form_values = $form_state->getValues();

    if (!empty($form_values['break_points']['table'])) {
      usort($form_values['break_points']['table'], [$this, 'sortByWeight']);
    }
    if (!empty($form_values['google_font_family']['table'])) {
      usort($form_values['google_font_family']['table'], [$this, 'sortByWeight']);
    }

    $config = $this->config('field_style.settings');
    $config->set('google_font_family_key', $form_values['google_font_family']['google_font_family_key']);
    $config->set('google_font_family', $form_values['google_font_family']['table']);
    $config->set('break_points', $form_values['break_points']['table']);

    if (!empty($form_values['break_points']['table'])) {
      foreach ($form_values['break_points']['table'] as $key => $value) {
        if (!empty($value['icon'][0])) {
          if ($file_id = $value['icon'][0]) {
            $file = \Drupal::entityTypeManager()->getStorage('file')->load($file_id);
            $file->setPermanent();
            $file->save();
          }
        }
      }
    }

    $config->save();
  }

  /**
   *
   */
  private  function sortByWeight($a, $b) {
    return ($a["weight"] <= $b["weight"]) ? -1 : 1;
  }

  /**
   *
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

}
