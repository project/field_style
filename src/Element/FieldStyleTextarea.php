<?php

namespace Drupal\field_style\Element;

use Drupal\Core\Render\Element;
use Drupal\Core\Render\Element\FormElement;
use Drupal\Core\Form\FormStateInterface;
use Drupal\field_style\FieldStyle;

/**
 * Provides a Textarea Style element.
 *
 * @FormElement("field_style")
 */
class FieldStyleTextarea extends FormElement {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $class = get_class($this);
    return [
      '#input' => TRUE,
      '#cols' => 60,
      '#rows' => 5,
      '#size' => 100,
      '#process' => [
        [$class, 'processAjaxForm'],
        [$class, 'processGroup'],
      ],
      '#pre_render' => [
        [$class, 'preRenderGroup'],
        [$class, 'preRenderStyle'],
      ],
      '#theme' => 'field_style',
      '#theme_wrappers' => ['form_element'],
    ];
  }

  /**
   *
   */
  public static function preRenderStyle(array $element) {
    $element['#attributes']['class'][] = 'field-style-started';
    $element['#attributes']['field-style-started'] = 'false';
    Element::setAttributes($element, [
      'id', 'name', 'value', 'class', 'size', 'field-style-started', 'cols', 'rows',
    ]);

    $break_points = FieldStyle::break_points();
    \Drupal::moduleHandler()->invokeAll('field_style_break_points', [&$break_points]);

    $google_fonts = FieldStyle::google_fonts();
    \Drupal::moduleHandler()->invokeAll('field_style_google_fonts', [&$google_fonts]);

    $custom_fonts = [];
    \Drupal::moduleHandler()->invokeAll('field_style_custom_font', [&$custom_fonts]);

    $google_fonts = array_merge($google_fonts, $custom_fonts);

    $global_style = [];
    \Drupal::moduleHandler()->invokeAll('field_style_global_style', [&$global_style]);

    $image_style = [];
    \Drupal::moduleHandler()->invokeAll('field_style_global_image_style', [&$image_style]);

    $element['#attached']['library'][] = 'field_style/global';
    $element['#attached']['drupalSettings']['field_style']['global_settings'] = [
      'break_points' => $break_points,
      'google_fonts' => $google_fonts,
      'global_style' => $global_style,
      'image_style' => $image_style,
    ];
    $element['#attached']['drupalSettings']['field_style']['fields'][$element['#id']] = [
      'states' => array_filter($element['#detail_states']),
      'properties' => array_filter($element['#properties']),
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public static function valueCallback(&$element, $input, FormStateInterface $form_state) {
    if ($input !== FALSE && $input !== NULL) {
      return is_scalar($input) ? (string) $input : '';
    }
    return NULL;
  }

}
