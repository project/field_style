<?php

namespace Drupal\field_style\Utils;

/**
 *
 */
class GoogleFonts {

  private $google_fonts = [];
  private $url = '';

  /**
   *
   */
  public function __construct($style) {
    $config = \Drupal::config('field_style.settings');
    $google_fonts = $config->get('google_fonts_storage');
    $google_fonts = $google_fonts['data']['items'];
    $this->google_fonts = $google_fonts;
    $this->style = $style;
  }

  /**
   *
   */
  public function build() {
    $matchedFonts = $this->get();
    $url = 'https://fonts.googleapis.com/css2?';
    $fonts_to_merge = [];
    $font_ = '';
    foreach ($matchedFonts as $family => $value) {
      if (!empty($value)) {
        $value = $value[0];
        $font = $this->getFont($family);
        if ($font) {
          $fonts_to_merge[$family] = [];
          if (count($font['variants'])) {
            foreach ($font['variants'] as $variant) {
              if (is_numeric($variant)) {
                $fonts_to_merge[$family][$value['weight']] = $value['style'];
                if (isset($fonts_to_merge[$family][$value['weight']]) && $value['style'] == 'italic') {
                  $fonts_to_merge[$family][$value['weight']] = TRUE;
                }
              }
            }
          }
        }
      }
    }

    if (count($fonts_to_merge)) {
      foreach ($fonts_to_merge as $family => $variations) {
        $f = 'wght';
        $weights = [];
        $italic_ = FALSE;
        $font_ .= 'family=' . $family;
        if (
              (in_array('regular', array_keys($variations)) && count($variations) == 1) ||
              (in_array('normal', array_keys($variations)) && count($variations) == 1) ||
              (in_array('normal', array_keys($variations)) && in_array('regular', array_keys($variations)) && count($variations) == 2)
              ) {
        }
        else {
          foreach ($variations as $weight => $italic) {
            if ($italic) {
              $italic_ = TRUE;
            }
            $weights[] = $weight . ';';
          }
          if (count($weights)) {
            $font_ .= ':';
            if (!$italic_) {
              foreach ($weights as $key => $value) {
                $weights[$key] = substr($value, 0, -1);
              }
              $font_ .= 'wght@';
              $font_ .= join(';', $weights);
            }
            else {
              foreach ($weights as $w) {
                $font_ .= 'ital,wght@';
                $font_ .= '0,' . $w;
                $font_ .= '1,' . $w;
              }
              $font_ = substr($font_, 0, -1);
            }
          }
        }
        $font_ .= "&";
      }
      $font_ = substr($font_, 0, -1);
    }

    return !$font_ ? FALSE : $url . $font_ . '&display=swap';
  }

  /**
   *
   */
  public function getUrl() {
    return $this->url;
  }

  /**
   *
   */
  private function getFont($font) {
    $family = NULL;
    if (!empty($this->google_fonts)) {
      foreach ($this->google_fonts as $value) {
        if ($value['family'] == $font) {
          $family = $value;
          break 1;
        }
      }
    }
    return $family;
  }

  /**
   *
   */
  private function get() {
    $fontFamily = [];
    foreach ($this->style as $break_point => $states) {
      foreach ($states as $state => $state_value) {
        if (isset($state_value->fontFamily) && $state_value->fontFamily) {
          if (!isset($fontFamily[$state_value->fontFamily])) {
            $fontFamily[$state_value->fontFamily] = [];
          }
          if (isset($state_value->fontWeight) && $state_value->fontWeight) {
            $fontStyle = '';
            if (isset($state_value->fontStyle) && $state_value->fontStyle) {
              $fontStyle = $state_value->fontStyle;
            }
            $fontFamily[$state_value->fontFamily][] = [
              'style' => $fontStyle,
              'weight' => $state_value->fontWeight,
            ];
          }
        }
      }
    }
    return $fontFamily;
  }

}
