<?php

namespace Drupal\field_style\Utils;

use Drupal\node\NodeInterface;
use Drupal\field_style\FieldStyle;

/**
 *
 */
class InlineStyle {

  /**
   *
   */
  public function __construct($object_style, $class_name) {
    $this->style = $object_style;
    $this->class_name = $class_name;
    $this->raw_style = '';
    $this->font_family = [];
  }

  /**
   *
   */
  public function build() {
    $break_points = FieldStyle::break_points();
    $this->raw_style = '';

    $global_style = [];
    \Drupal::moduleHandler()->invokeAll('field_style_global_style', [&$global_style]);

    if (!empty($this->style->globalProperty->global_style) && $global_style[$this->style->globalProperty->global_style]) {
      $global_style = $global_style[$this->style->globalProperty->global_style];
      unset($this->style->globalProperty);
      if (empty((Array) $this->style)) {
        $this->style = $global_style['style'];
      }
    }

    foreach ($this->style as $break_point => $states) {
      $states = (Array) $states;
      if (strtolower($break_point) !== 'default') {
        $this->raw_style .= trim($break_points[($break_point)]['media_queries']) . '{';
      }
      $keys_states = array_keys($states);
      if (!empty($global_style['style'][$break_point])) {
        $keys_global = array_keys($global_style['style'][$break_point]);
        $keys_states = array_merge($keys_states, $keys_global);
        $keys_states = array_unique($keys_states);
      }
      foreach ($keys_states as $index => $state) {
        $properties = [];
        if (isset($states[$state])) {
          $properties = $states[$state];
        }
        /**
         * Clear selector when state is default.
         */
        $state_ = $state == 'default' ? '' : $state;
        $root = $this->sanitizeSelector($this->class_name, $state_);
        $this->raw_style .= $root . " {";

        $properties = (Array) $properties;
        $properties = array_filter($properties);

        if (!empty($global_style['style'][$break_point][$state])) {
          $properties = $this->mergeProperties($global_style['style'][$break_point][$state], $properties);
        }

        foreach ($properties as $property => $value) {
          $v = $this->call_hook_property($property, $value);
          if ($property && $v) {
            $this->raw_style .= strtolower(
              preg_replace(
                  ["/([A-Z]+)/", "/_([A-Z]+)([A-Z][a-z])/"],
                  ["-$1", "-$1-$2"],
                  lcfirst($property)
              )
            ) . ":{$v};";
            if ($property == 'fontFamily') {
              $this->font_family[$index]['fontFamily'] = $v;
            }
            if ($property == 'fontWeight') {
              $this->font_family[$index]['variant']['fontWeight'] = $v;
            }
            if ($property == 'fontStyle') {
              $this->font_family[$index]['variant']['fontStyle'] = $v;
            }
          }
        }

        $this->raw_style .= '}';
      }
      if (strtolower($break_point) !== 'default') {
        $this->raw_style .= '}';
      }
    }
    return preg_replace('/(?:[^\r\n,{}]+)(?:,(?=[^}]*{)|\s*{[\s]*})/', '', $this->raw_style);
  }

  /**
   *
   */
  public function get_font_family_used() {
    return $this->font_family ?? FALSE;
  }

  /**
   *
   */
  private function call_hook_property($property, $value) {
    $property = strtolower(
      preg_replace(
          ["/([A-Z]+)/", "/_([A-Z]+)([A-Z][a-z])/"],
          ["_$1", "_$1_$2"],
          lcfirst($property)
        )
      );

    $val = [];
    $val['#value'] = $value;
    \Drupal::moduleHandler()->invokeAll('field_style_property', [&$val]);
    \Drupal::moduleHandler()->invokeAll('field_style_property_' . $property, [&$val]);

    $value = $val['value'];

    return $value;
  }

  /**
   *
   */
  private function mergeProperties($original, $overwrite) {
    $original = array_filter($original);
    $overwrite = array_filter($overwrite);
    return array_merge($original, $overwrite);
  }

  /**
   *
   */
  private function sanitizeSelector($selector, $state) {
    $node = \Drupal::routeMatch()->getParameter('node');
    if ($node instanceof NodeInterface) {
      $token = \Drupal::token();
      $selector = $token->replace($selector, ['node' => $node]);
    }

    return str_replace('[state]', ($state == '' ? '' : ':' . $state), $selector);
  }

  /**
   *
   */
  public function getHash() {
    return md5($this->raw_style);
  }

  /**
   *
   */
  public function getValue() {
    return $this->raw_style;
  }

}
