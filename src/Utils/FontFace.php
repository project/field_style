<?php

namespace Drupal\field_style\Utils;

/**
 *
 */
class FontFace {

  /**
   *
   */
  public function __construct($fonts) {
    $custom_fonts = [];
    \Drupal::moduleHandler()->invokeAll('field_style_custom_font', [&$custom_fonts]);

    $this->fonts = $fonts;
    $this->custom_fonts = $custom_fonts;
  }

  /**
   *
   */
  public function build() {
    $css = '';

    $fonts = [];
    if ($this->fonts) {
      foreach ($this->fonts as $fontFamily) {
        if (!empty($this->custom_fonts[$fontFamily['fontFamily']])) {
          if (empty($fonts[$fontFamily['fontFamily']])) {
            $fonts[$fontFamily['fontFamily']] = [];
          }
          $variant = $fontFamily['variant']['fontWeight'];
          if (isset($fontFamily['variant']['fontStyle']) && $fontFamily['variant']['fontStyle'] == 'italic') {
            $variant .= 'italic';
          }
          if (!in_array($variant, $fonts[$fontFamily['fontFamily']])) {
            $fonts[$fontFamily['fontFamily']][] = $variant;
          }
        }
      }
    }

    if (!empty($fonts)) {
      foreach ($fonts as $family => $variants) {
        $src = [];
        foreach ($variants as $variant) {
          if (!empty($this->custom_fonts[$family]['files'][$variant])) {
            foreach ($this->custom_fonts[$family]['files'][$variant] as $ext => $path) {
              if ($ext == 'eot') {
                $src[] = "url('" . $path . "')";
                $src[] = "url('" . $path . '?#iefix' . "') format('embedded-opentype')";
              }
              if ($ext == 'woff') {
                $src[] = "url('" . $path . "') format('woff')";
              }
              if ($ext == 'woff2') {
                $src[] = "url('" . $path . "') format('woff2')";
              }
              if ($ext == 'ttf') {
                $src[] = "url('" . $path . "') format('truetype')";
              }
              if ($ext == 'svg') {
                $src[] = "url('" . $path . "#svgFontName') format('svg')";
              }
            }
          }

          if ($src) {
            $css .= "@font-face {";
            if (strpos($variant, 'italic')) {
              $css .= 'font-style: italic;';
            }
            if (strpos($variant, 'normal')) {
              $css .= 'font-style: normal;';
            }
            if (strpos($variant, 'regular')) {
              $css .= 'font-style: regular;';
            }
            $css .= "font-family: '" . $family . "';";
            $css .= "src: " . $src[0] . ";";
            unset($src[0]);
            if ($src) {
              $css .= "src: " . implode(',', $src) . ";";
            }
            $css .= "}";
          }
        }
      }
    }
    return $css;
  }

}
