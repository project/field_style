<?php

namespace Drupal\field_style;

use Drupal\file\Entity\File;

/**
 *
 */
class FieldStyle {

  /**
   *
   */
  public static function break_points() {
    $config = \Drupal::config('field_style.settings');
    $break_ponts = $config->get('break_points');
    $break_ponts_ = [];
    if (!empty($break_ponts)) {
      foreach ($break_ponts as $key => $value) {
        if (!empty($value['icon'])) {
          $file = File::load($value['icon'][0]);
          if ($file instanceof File && file_exists($file->getFileUri())) {
            $break_ponts[$key]['icon'] = $file->url();
          }
          else {
            $break_ponts[$key]['icon'] = '';
          }
        }
        $break_ponts_[$value['name']] = $break_ponts[$key];
      }
    }

    return $break_ponts_;
  }

  /**
   *
   */
  public static function google_fonts() {
    $config = \Drupal::config('field_style.settings');
    $google_fonts = $config->get('google_fonts_storage');
    $fonts = $config->get('google_font_family');
    $fonts_use = [];
    if (!empty($fonts) && !empty($google_fonts['data']['items'])) {
      foreach ($fonts as $key => $f) {
        foreach ($google_fonts['data']['items'] as $key => $value) {
          if ($f['name'] == $value['family']) {
            $fonts_use[] = [
              'family' => $value['family'],
              'variants' => $value['variants'],
            ];
          }
        }
      }
    }

    return $fonts_use;
  }

}
