<?php

namespace Drupal\field_style\Plugin\Field\FieldFormatter;

use Drupal\Component\Utility\Html;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'field_style_formatter_type' formatter.
 *
 * @FieldFormatter(
 *   id = "field_style_formatter_type",
 *   label = @Translation("Style"),
 *   field_types = {
 *    "field_style"
 *    }
 * )
 */
class FieldStyleFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'class_selector_name' => '',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {

    $elements['class_selector_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Class Name'),
      '#default_value' => $this->getSetting('class_selector_name') ? $this->getSetting('class_selector_name') : '',
      '#description' => $this->t('Use the <b>[state]</b> token where you want to apply the state.'),
    ];

    $elements['token_tree_link'] = [
      '#theme' => 'token_tree_link',
      '#token_types' => ['node'],
      '#show_restricted' => TRUE,
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    foreach ($items as $delta => $item) {
      $elements[$delta] = [
        '#markup' => $this->viewValue($item),
        '#raw_value' => $item->value,
        '#value' => json_decode($item->value),
        '#class_selector' => $this->getSettings('class_selector_name'),
      ];
    }

    return $elements;
  }

  /**
   * Generate the output appropriate for one field item.
   *
   * @param \Drupal\Core\Field\FieldItemInterface $item
   *   One field item.
   *
   * @return string
   *   The textual output generated.
   */
  protected function viewValue(FieldItemInterface $item) {
    // The text value has no text format assigned to it, so the user input
    // should equal the output, including newlines.
    return nl2br(Html::escape($item->value));
  }

}
