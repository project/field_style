<?php

namespace Drupal\field_style\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'field_style_widget' widget.
 *
 * @FieldWidget(
 *   id = "field_style_widget",
 *   module = "field_style",
 *   label = @Translation("Style"),
 *   field_types = {
 *      "field_style"
 *   }
 * )
 */
class FieldStyleWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'properties' => [
        'fontFamily' => FALSE,
        'fontSize' => FALSE,
        'fontWeight' => FALSE,
        'fontStyle' => FALSE,
        'textAlign' => FALSE,
        'textDecoration' => FALSE,
        'letterSpacing' => FALSE,
        'lineHeight' => FALSE,
        'textIndent' => FALSE,
        'textTransform' => FALSE,
        'textAlignLast' => FALSE,
        'width' => FALSE,
        'height' => FALSE,
        'margin' => FALSE,
        'marginTop' => FALSE,
        'marginLeft' => FALSE,
        'marginRight' => FALSE,
        'marginBottom' => FALSE,
        'padding' => FALSE,
        'paddingTop' => FALSE,
        'paddingLeft' => FALSE,
        'paddingRight' => FALSE,
        'paddingBottom' => FALSE,
        'color' => FALSE,
        'fill' => FALSE,
        'backgroundImage' => FALSE,
        'backgroundRepeat' => FALSE,
        'backgroundRepeatX' => FALSE,
        'backgroundRepeatY' => FALSE,
        'backgroundPosition' => FALSE,
        'backgroundPositionX' => FALSE,
        'backgroundPositionY' => FALSE,
        'backgroundSize' => FALSE,
        'backgroundColor' => FALSE,
        'borderRadius' => FALSE,
        'border' => FALSE,
        'borderStyle' => FALSE,
        'borderWidth' => FALSE,
        'borderTop' => FALSE,
        'borderLeft' => FALSE,
        'borderRight' => FALSE,
        'borderBottom' => FALSE,
        'borderColor' => FALSE,
        'borderTopColor' => FALSE,
        'borderTopWidth' => FALSE,
        'borderTopStyle' => FALSE,
        'borderLeftColor' => FALSE,
        'borderLeftWidth' => FALSE,
        'borderLeftStyle' => FALSE,
        'borderRightColor' => FALSE,
        'borderRightWidth' => FALSE,
        'borderRightStyle' => FALSE,
        'borderBottomColor' => FALSE,
        'borderBottomWidth' => FALSE,
        'borderBottomStyle' => FALSE,
        'alignContent' => FALSE,
        'alignItems' => FALSE,
        'alignSelf' => FALSE,
        'bottom' => FALSE,
        'display' => FALSE,
        'float' => FALSE,
        'left' => FALSE,
        'maxHeight' => FALSE,
        'maxWidth' => FALSE,
        'minHeight' => FALSE,
        'top' => FALSE,
        'order' => FALSE,
        'position' => FALSE,
        'verticalAlign' => FALSE,
        'visibility' => FALSE,
        'whiteSpace' => FALSE,
        'wordBreak' => FALSE,
        'wordSpacing' => FALSE,
        'wordWrap' => FALSE,
        'zIndex' => FALSE,
        'cursor' => FALSE,

        'animation' => FALSE,
        'animationDelay' => FALSE,
        'animationDirection' => FALSE,
        'animationFillMode' => FALSE,
        'animationInterationCount' => FALSE,
        'animationName' => FALSE,
        'animationPlayState' => FALSE,
        'animationTimingFunction' => FALSE,
        'backfaceVisibility' => FALSE,
        'background' => FALSE,
        'backgroundAttachment' => FALSE,
        'backgroundClip' => FALSE,
        'backgroundOrigin' => FALSE,
        'boxShadow' => FALSE,
        'boxSizing' => FALSE,
        'flexDirection' => FALSE,
        'flexWrap' => FALSE,
        'justifyContent' => FALSE,
        'overflow' => FALSE,
        'overflowX' => FALSE,
        'overflowY' => FALSE,
        'textShadow' => FALSE,
        'transition' => FALSE,
        'transitionTimingFunction' => FALSE,
      ],
      'detail_states' => [],
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = [];

    $elements['detail_states'] = [
      '#type' => 'details',
      '#title' => $this->t('States'),
    ];

    $elements['detail_states']['default'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Default'),
      '#default_value' => !isset($this->getSetting('detail_states')['default']) ? TRUE : $this->getSetting('detail_states')['default'],
    ];

    $elements['detail_states']['hover'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Hover'),
      '#default_value' => isset($this->getSetting('detail_states')['hover']) ? $this->getSetting('detail_states')['hover'] : '',
    ];

    $elements['detail_states']['active'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Active'),
      '#default_value' => isset($this->getSetting('detail_states')['active']) ? $this->getSetting('detail_states')['active'] : '',
    ];

    $elements['detail_states']['focus'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Focus'),
      '#default_value' => isset($this->getSetting('detail_states')['focus']) ? $this->getSetting('detail_states')['focus'] : '',
    ];

    $elements['detail_states']['visited'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Visited'),
      '#default_value' => isset($this->getSetting('detail_states')['visited']) ? $this->getSetting('detail_states')['visited'] : '',
    ];

    $elements['properties'] = [
      '#type' => 'details',
      '#title' => $this->t('properties'),
    ];

    $fields = ['globalStyle', 'fontFamily', 'fontSize', 'fontWeight', 'fontStyle', 'textAlign', 'textDecoration', 'letterSpacing', 'lineHeight', 'textIndent', 'textTransform', 'textAlignLast', 'width', 'height', 'margin', 'marginTop', 'marginLeft', 'marginRight', 'marginBottom', 'padding', 'paddingTop', 'paddingLeft', 'paddingRight', 'paddingBottom', 'color', 'fill', 'backgroundImage', 'backgroundRepeat', 'backgroundRepeatX', 'backgroundRepeatY', 'backgroundPosition', 'backgroundPositionX', 'backgroundPositionY', 'backgroundSize', 'backgroundColor', 'borderRadius', 'border', 'borderStyle', 'borderWidth', 'borderTop', 'borderLeft', 'borderRight', 'borderBottom', 'borderColor', 'borderTopColor', 'borderTopWidth', 'borderTopStyle', 'borderLeftColor', 'borderLeftWidth', 'borderLeftStyle', 'borderRightColor', 'borderRightWidth', 'borderRightStyle', 'borderBottomColor', 'borderBottomWidth', 'borderBottomStyle', 'alignContent', 'alignItems', 'alignSelf', 'bottom', 'display', 'float', 'left', 'maxHeight', 'maxWidth', 'minHeight', 'top', 'order', 'position', 'verticalAlign', 'visibility', 'whiteSpace', 'wordBreak', 'wordSpacing', 'wordWrap', 'zIndex', 'cursor', 'animation', 'animationDelay', 'animationDirection', 'animationFillMode', 'animationInterationCount', 'animationName', 'animationPlayState', 'animationTimingFunction', 'backfaceVisibility', 'background', 'backgroundAttachment', 'backgroundClip', 'backgroundOrigin', 'boxShadow', 'boxSizing', 'flexDirection', 'flexWrap', 'justifyContent', 'overflow', 'overflowX', 'overflowY', 'textShadow', 'transition', 'transitionTimingFunction'];

    foreach ($fields as $key) {
      $elements['properties'][$key] = [
        '#type' => 'checkbox',
        '#title' => $this->t($key),
        '#default_value' => isset($this->getSetting('properties')[$key]) ? $this->getSetting('properties')[$key] : '',
      ];
    }

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element['value'] = $element + [
      '#type' => 'field_style',
      '#default_value' => $items[$delta]->value,
      '#attributes' => ['class' => ['js-text-full', 'text-full']],
      '#class' => [],
      '#properties' => [
        'globalStyle' => $this->getSetting('properties')['globalStyle'] ?? '' ?? [],
        'fontFamily' => $this->getSetting('properties')['fontFamily'] ?? '',
        'fontSize' => $this->getSetting('properties')['fontSize'] ?? '',
        'fontWeight' => $this->getSetting('properties')['fontWeight'] ?? '',
        'fontStyle' => $this->getSetting('properties')['fontStyle'] ?? '',
        'textAlign' => $this->getSetting('properties')['textAlign'] ?? '',
        'textDecoration' => $this->getSetting('properties')['textDecoration'] ?? '',
        'letterSpacing' => $this->getSetting('properties')['letterSpacing'] ?? '',
        'lineHeight' => $this->getSetting('properties')['lineHeight'] ?? '',
        'textIndent' => $this->getSetting('properties')['textIndent'] ?? '',
        'textTransform' => $this->getSetting('properties')['textTransform'] ?? '',
        'textAlignLast' => $this->getSetting('properties')['textAlignLast'] ?? '',

        'width' => $this->getSetting('properties')['width'] ?? '',
        'height' => $this->getSetting('properties')['height'] ?? '',

        'margin' => $this->getSetting('properties')['margin'] ?? '',
        'marginTop' => $this->getSetting('properties')['marginTop'] ?? '',
        'marginLeft' => $this->getSetting('properties')['marginLeft'] ?? '',
        'marginRight' => $this->getSetting('properties')['marginRight'] ?? '',
        'marginBottom' => $this->getSetting('properties')['marginBottom'] ?? '',

        'padding' => $this->getSetting('properties')['padding'] ?? '',
        'paddingTop' => $this->getSetting('properties')['paddingTop'] ?? '',
        'paddingLeft' => $this->getSetting('properties')['paddingLeft'] ?? '',
        'paddingRight' => $this->getSetting('properties')['paddingRight'] ?? '',
        'paddingBottom' => $this->getSetting('properties')['paddingBottom'] ?? '',

        'color' => $this->getSetting('properties')['color'] ?? '',
        'fill' => $this->getSetting('properties')['fill'] ?? '',

        'backgroundRepeat' => $this->getSetting('properties')['backgroundRepeat'] ?? '',
        'backgroundRepeatX' => $this->getSetting('properties')['backgroundRepeatX'] ?? '',
        'backgroundRepeatY' => $this->getSetting('properties')['backgroundRepeatY'] ?? '',
        'backgroundPosition' => $this->getSetting('properties')['backgroundPosition'] ?? '',
        'backgroundImage' => $this->getSetting('properties')['backgroundImage'] ?? '',
        'backgroundPositionX' => $this->getSetting('properties')['backgroundPositionX'] ?? '',
        'backgroundPositionY' => $this->getSetting('properties')['backgroundPositionY'] ?? '',
        'backgroundSize' => $this->getSetting('properties')['backgroundSize'] ?? '',
        'backgroundColor' => $this->getSetting('properties')['backgroundColor'] ?? '',

        'borderRadius' => $this->getSetting('properties')['borderRadius'] ?? '',
        'border' => $this->getSetting('properties')['border'] ?? '',
        'borderStyle' => $this->getSetting('properties')['borderStyle'] ?? '',
        'borderWidth' => $this->getSetting('properties')['borderWidth'] ?? '',
        'borderTop' => $this->getSetting('properties')['borderTop'] ?? '',
        'borderLeft' => $this->getSetting('properties')['borderLeft'] ?? '',
        'borderRight' => $this->getSetting('properties')['borderRight'] ?? '',
        'borderBottom' => $this->getSetting('properties')['borderBottom'] ?? '',
        'borderColor' => $this->getSetting('properties')['borderColor'] ?? '',
        'borderTopColor' => $this->getSetting('properties')['borderTopColor'] ?? '',
        'borderTopWidth' => $this->getSetting('properties')['borderTopWidth'] ?? '',
        'borderTopStyle' => $this->getSetting('properties')['borderTopStyle'] ?? '',
        'borderLeftColor' => $this->getSetting('properties')['borderLeftColor'] ?? '',
        'borderLeftWidth' => $this->getSetting('properties')['borderLeftWidth'] ?? '',
        'borderLeftStyle' => $this->getSetting('properties')['borderLeftStyle'] ?? '',
        'borderRightColor' => $this->getSetting('properties')['borderRightColor'] ?? '',
        'borderRightWidth' => $this->getSetting('properties')['borderRightWidth'] ?? '',
        'borderRightStyle' => $this->getSetting('properties')['borderRightStyle'] ?? '',
        'borderBottomColor' => $this->getSetting('properties')['borderBottomColor'] ?? '',
        'borderBottomWidth' => $this->getSetting('properties')['borderBottomWidth'] ?? '',
        'borderBottomStyle' => $this->getSetting('properties')['borderBottomStyle'] ?? '',

        'alignContent' => $this->getSetting('properties')['alignContent'] ?? '',
        'alignItems' => $this->getSetting('properties')['alignItems'] ?? '',
        'alignSelf' => $this->getSetting('properties')['alignSelf'] ?? '',
        'bottom' => $this->getSetting('properties')['bottom'] ?? '',
        'display' => $this->getSetting('properties')['display'] ?? '',
        'float' => $this->getSetting('properties')['float'] ?? '',
        'left' => $this->getSetting('properties')['left'] ?? '',
        'maxHeight' => $this->getSetting('properties')['maxHeight'] ?? '',
        'maxWidth' => $this->getSetting('properties')['maxWidth'] ?? '',
        'minHeight' => $this->getSetting('properties')['minHeight'] ?? '',
        'top' => $this->getSetting('properties')['top'] ?? '',
        'order' => $this->getSetting('properties')['order'] ?? '',
        'position' => $this->getSetting('properties')['position'] ?? '',
        'verticalAlign' => $this->getSetting('properties')['verticalAlign'] ?? '',
        'visibility' => $this->getSetting('properties')['visibility'] ?? '',
        'whiteSpace' => $this->getSetting('properties')['whiteSpace'] ?? '',
        'wordBreak' => $this->getSetting('properties')['wordBreak'] ?? '',
        'wordSpacing' => $this->getSetting('properties')['wordSpacing'] ?? '',
        'wordWrap' => $this->getSetting('properties')['wordWrap'] ?? '',
        'zIndex' => $this->getSetting('properties')['zIndex'] ?? '',
        'cursor' => $this->getSetting('properties')['cursor'] ?? '',

        'animation' => $this->getSetting('properties')['animation'] ?? '',
        'animationDelay' => $this->getSetting('properties')['animationDelay'] ?? '',
        'animationDirection' => $this->getSetting('properties')['animationDirection'] ?? '',
        'animationFillMode' => $this->getSetting('properties')['animationFillMode'] ?? '',
        'animationInterationCount' => $this->getSetting('properties')['animationInterationCount'] ?? '',
        'animationName' => $this->getSetting('properties')['animationName'] ?? '',
        'animationPlayState' => $this->getSetting('properties')['animationPlayState'] ?? '',
        'animationTimingFunction' => $this->getSetting('properties')['animationTimingFunction'] ?? '',
        'backfaceVisibility' => $this->getSetting('properties')['backfaceVisibility'] ?? '',
        'background' => $this->getSetting('properties')['background'] ?? '',
        'backgroundAttachment' => $this->getSetting('properties')['backgroundAttachment'] ?? '',
        'backgroundClip' => $this->getSetting('properties')['backgroundClip'] ?? '',
        'backgroundOrigin' => $this->getSetting('properties')['backgroundOrigin'] ?? '',
        'boxShadow' => $this->getSetting('properties')['boxShadow'] ?? '',
        'boxSizing' => $this->getSetting('properties')['boxSizing'] ?? '',
        'flexDirection' => $this->getSetting('properties')['flexDirection'] ?? '',
        'flexWrap' => $this->getSetting('properties')['flexWrap'] ?? '',
        'justifyContent' => $this->getSetting('properties')['justifyContent'] ?? '',
        'overflow' => $this->getSetting('properties')['overflow'] ?? '',
        'overflowX' => $this->getSetting('properties')['overflowX'] ?? '',
        'overflowY' => $this->getSetting('properties')['overflowY'] ?? '',
        'textShadow' => $this->getSetting('properties')['textShadow'] ?? '',
        'transition' => $this->getSetting('properties')['transition'] ?? '',
        'transitionTimingFunction' => $this->getSetting('properties')['transitionTimingFunction'] ?? '',

      ],
      '#detail_states' => $this->getSetting('detail_states'),
      '#weight' => 0,
    ];

    return $element;
  }

}
