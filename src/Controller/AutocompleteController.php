<?php

namespace Drupal\field_style\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Component\Utility\Tags;
use Drupal\Component\Utility\Unicode;

/**
 * Defines a route controller for entity autocomplete form elements.
 */
class AutocompleteController extends ControllerBase {

  /**
   * Handler for autocomplete request.
   */
  public function handleAutocomplete(Request $request, $field_name, $count) {
    $config = \Drupal::config('field_style.settings');
    $config = $config->get('google_fonts_storage');

    $fonts = $this->get_fonts();
    $results = [];

    if ($input = $request->query->get('q')) {
      $typed_string = Tags::explode($input);
      $typed_string = Unicode::strtolower(array_pop($typed_string));

      $count_matches = 0;
      if (count($fonts) && isset($fonts['items'])) {
        foreach ($fonts['items'] as $key => $value) {
          $search = $input;
          if (preg_match("/{$search}/i", $value['family'])) {
            $results[] = [
              'value' => $value['family'],
              'label' => $value['family'],
            ];
            $count_matches++;
          }
          if ($count_matches > $count) {
            break;
          }
        }
      }

    }
    return new JsonResponse($results);
  }

  /**
   *
   */
  private function get_fonts() {
    $config = \Drupal::service('config.factory')->getEditable('field_style.settings');
    $google_fonts = $config->get('google_fonts_storage');
    $google_api_key = $config->get('google_font_family_key');
    if ($google_api_key) {

      if (!isset($google_fonts['data']) || (isset($google_fonts['last_update']) &&  date('Y-m-d H:i:s') > date('Y-m-d H:i:s', strtotime('+7 day', strtotime($google_fonts['last_update']))))) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://www.googleapis.com/webfonts/v1/webfonts?sort=alpha&key=" . $google_api_key);
        curl_setopt($ch, CURLOPT_HTTPHEADER, ["Content-Type: application/json"]);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        $fonts_list = json_decode(curl_exec($ch), TRUE);
        $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        if ($http_code != 200) {
          exit('Error : Failed to get Google Fonts list');
        }

        if ($fonts_list) {
          $config->set('google_fonts_storage', [
            'data' => $fonts_list,
            'last_update' => date('Y-m-d H:i:s'),
          ])
            ->save();
          return $fonts_list;
        }
      }
      elseif (isset($google_fonts['data']) && $google_fonts['data']) {
        return $google_fonts['data'];
      }
    }
  }

}









// https://usefulangle.com/post/260/google-fonts-api-php
// Array (
//     [kind] => webfonts#webfont
//     [family] => Roboto
//     [category] => sans-serif
//     [variants] => Array
//         (
//             [0] => 100
//             [1] => 100italic
//             [2] => 300
//             [3] => 300italic
//             [4] => regular
//             [5] => italic
//             [6] => 500
//             [7] => 500italic
//             [8] => 700
//             [9] => 700italic
//             [10] => 900
//             [11] => 900italic
//         )
// [subsets] => Array
//         (
//             [0] => greek-ext
//             [1] => latin-ext
//             [2] => latin
//             [3] => cyrillic-ext
//             [4] => vietnamese
//             [5] => cyrillic
//             [6] => greek
//         )
// [version] => v20
//     [lastModified] => 2019-07-24
//     [files] => Array
//         (
//             [100] => http://fonts.gstatic.com/s/roboto/v20/KFOkCnqEu92Fr1MmgWxPKTM1K9nz.ttf
//             [100italic] => http://fonts.gstatic.com/s/roboto/v20/KFOiCnqEu92Fr1Mu51QrIzcXLsnzjYk.ttf
//             [300] => http://fonts.gstatic.com/s/roboto/v20/KFOlCnqEu92Fr1MmSU5vAx05IsDqlA.ttf
//             [300italic] => http://fonts.gstatic.com/s/roboto/v20/KFOjCnqEu92Fr1Mu51TjARc9AMX6lJBP.ttf
//             [regular] => http://fonts.gstatic.com/s/roboto/v20/KFOmCnqEu92Fr1Me5WZLCzYlKw.ttf
//             [italic] => http://fonts.gstatic.com/s/roboto/v20/KFOkCnqEu92Fr1Mu52xPKTM1K9nz.ttf
//             [500] => http://fonts.gstatic.com/s/roboto/v20/KFOlCnqEu92Fr1MmEU9vAx05IsDqlA.ttf
//             [500italic] => http://fonts.gstatic.com/s/roboto/v20/KFOjCnqEu92Fr1Mu51S7ABc9AMX6lJBP.ttf
//             [700] => http://fonts.gstatic.com/s/roboto/v20/KFOlCnqEu92Fr1MmWUlvAx05IsDqlA.ttf
//             [700italic] => http://fonts.gstatic.com/s/roboto/v20/KFOjCnqEu92Fr1Mu51TzBhc9AMX6lJBP.ttf
//             [900] => http://fonts.gstatic.com/s/roboto/v20/KFOlCnqEu92Fr1MmYUtvAx05IsDqlA.ttf
//             [900italic] => http://fonts.gstatic.com/s/roboto/v20/KFOjCnqEu92Fr1Mu51TLBBc9AMX6lJBP.ttf
//         )
// )
