<?php

namespace Drupal\field_style\Controller;

use Drupal\file\Entity\File;
use Drupal\user\Entity\User;
use Drupal\clamav\Scanner;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Session\AccountProxyInterface;
use GuzzleHttp\ClientInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelFactory;

/**
 * Class UploadController.
 */
class UploadController extends ControllerBase {

  /**
   * The watchdog log.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;


  /**
   * A current user instance.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * The current request.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $currentRequest;

  /**
   * A configuration object.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * The http client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * End-point path.
   *
   * @var string
   */
  protected $path = '/field-style/attachments';
  /**
   * The body is read from a stream.
   *
   * @var string
   */
  protected $responseBody;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    LoggerChannelFactory $logger,
    ConfigFactoryInterface $config,
    AccountProxyInterface $current_user,
    Request $current_request,
    ClientInterface $http_client
  ) {
    $this->logger = $logger;
    $this->config = $config;
    $this->currentUser = $current_user;
    $this->currentRequest = $current_request;
    $this->httpClient = $http_client;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('logger.factory'),
      $container->get('config.factory'),
      $container->get('current_user'),
      $container->get('request_stack')->getCurrentRequest(),
      $container->get('http_client'),
    );
  }

  /**
   * Handle the file upload.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   */
  public function upload() {
    try {
      $user = User::load(\Drupal::currentUser()->id());
      $fileData = $this->checkForVirus();
      $file = $this->saveFile();
      $response[] = [
        'id' => (Int) $file->id(),
        'url' => $file->createFileUrl(),
      ];
    }
    catch (Exception $e) {
      $response = [
        'error' => $e->getMessage(),
      ];
      return JsonResponse::create($response, $e->getStatusCode());
    }

    $statusCode = 500;
    if (!empty($response)) {
      $statusCode = 200;
    }
    return JsonResponse::fromJsonString(json_encode($response), $statusCode);
  }

  /**
   * Handle the file upload.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   */
  public function remove($fid) {
    $file = File::load($fid);
    if ($file != NULL) {
      try {
        // $file->delete();
        $response = [
          'status' => TRUE,
        ];
      }
      catch (Exception $e) {
        $response = [
          'error' => $e->getMessage(),
        ];
        return JsonResponse::create($response, $e->getStatusCode());
      }
    }

    $statusCode = 500;
    if (!empty($response)) {
      $statusCode = 200;
    }
    return JsonResponse::fromJsonString(json_encode($response), $statusCode);

  }

  /**
   * Handle the file upload.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   */
  public function list() {
    try {
      // $user = \Drupal\user\Entity\User::load(\Drupal::currentUser()->id());
      $response = [];
      $fids = $this->currentRequest->get('fids');

      foreach ($fids as $key => $fid) {
        $file = File::load($fid);
        if (!empty($file)) {
          $response[] = [
            'id' => $file->id(),
            'url' => $file->createFileUrl(),
          ];
        }
      }
    }
    catch (Exception $e) {
      $response = [
        'error' => $e->getMessage(),
      ];
      return JsonResponse::create($response, $e->getStatusCode());
    }

    $statusCode = 500;
    if (!empty($response)) {
      $statusCode = 200;
    }
    return JsonResponse::fromJsonString(json_encode($response), $statusCode);
  }

  /**
   *
   */
  protected function saveFile() {
    $files = $this->currentRequest->files;
    if ($files->count() == 0) {
      return FALSE;
    }
    $keys = $files->keys();
    /** @var \Symfony\Component\HttpFoundation\File\UploadedFile $uploadedFile */
    $uploadedFile = $this->currentRequest->files->get($keys[0])[0];

    $fileName = $uploadedFile->getClientOriginalName();
    if (empty($fileName)) {
      throw new HttpException(400, 'File name not found');
    }

    $path = $uploadedFile->getPathName();

    $fileData = file_get_contents($path, FILE_USE_INCLUDE_PATH);
    if (FALSE === $fileData) {
      throw new HttpException(400, 'File could not be processed');
    }

    $file = file_save_data($fileData, 'public://' . $this->path . '/' . $fileName, FILE_EXISTS_REPLACE);
    if (!empty($file)) {
      return $file;
    }

    return FALSE;
  }

  /**
   * Check for a virus in uploaded file.
   *
   * @return bool|string
   */
  protected function checkForVirus() {
    $files = $this->currentRequest->files;
    if ($files->count() == 0) {
      return FALSE;
    }
    $keys = $files->keys();
    /** @var \Symfony\Component\HttpFoundation\File\UploadedFile $uploadedFile */
    $uploadedFile = $this->currentRequest->files->get($keys[0])[0];

    $fileName = $uploadedFile->getClientOriginalName();
    if (empty($fileName)) {
      throw new HttpException(400, 'File name not found');
    }

    $path = $uploadedFile->getPathName();
    $fileData = file_get_contents($path, FILE_USE_INCLUDE_PATH);
    if (FALSE === $fileData) {
      throw new HttpException(400, 'File could not be processed');
    }

    if (\Drupal::service('module_handler')->moduleExists('clamav')) {
      $fileName = uniqid(rand());
      /** @var \Drupal\file\FileInterface $file */
      $file = file_save_data($fileData, 'public://' . $fileName, FILE_EXISTS_REPLACE);
      /** @var \Drupal\clamav\Scanner $clamAV */
      $clamAV = \Drupal::service('clamav');
      $result = $clamAV->scan($file);
      try {
        $file->delete();
      }
      catch (EntityStorageException $e) {
        $this->logger->error($e->getMessage());
      }

      if ($result == Scanner::FILE_IS_INFECTED) {
        throw new HttpException(403, 'Virus found');
      }
    }

    return $fileData;
  }

  /**
   * Maps a file name with extension to a mimetype.
   *
   * @param string $fileName
   *   The file file name.
   *
   * @return string|null
   * @link http://svn.apache.org/repos/asf/httpd/httpd/branches/1.3.x/conf/mime.types
   */
  protected function mimetypeFromFileName($fileName) {
    $splitName = explode('.', $fileName);
    if (count($splitName) < 2) {
      return NULL;
    }

    $mimetypes = [
      'jpeg' => 'image/jpeg',
      'jpg' => 'image/jpeg',
      'png' => 'image/png',
      'svg' => 'image/svg+xml',
    ];

    $extension = end($splitName);
    $extension = strtolower($extension);

    return isset($mimetypes[$extension]) ? $mimetypes[$extension] : NULL;
  }

}
