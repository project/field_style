import Vue from 'vue';
import App from './App';
import * as Settings from './settings.js'

function start() {

  const items = drupalSettings.field_style.fields;
  if (items) {
    for (let id in items) {
      var item = items[id];
      const textarea = document.querySelector('#' + id);
      const div_vue = document.querySelector('#vue-' + id);
      if (textarea && div_vue) {
        textarea.style.display = 'none';
        new Vue({
          render: h => h(App),
          data() {
            return {
              el: textarea,
              settings: item,
              global_settings: drupalSettings.field_style.global_settings
            }
          }
        }).$mount('#vue-' + id)
      }
    }
  }

}

(function (Drupal, drupalSettings) {
  Drupal.behaviors.field_style = {
    attach: function (context, settings) {
      start();
    }
  };
})(Drupal, drupalSettings);
