const { resolve } = require('path')
const { CleanWebpackPlugin } = require('clean-webpack-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const { VueLoaderPlugin } = require("vue-loader");
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const SassLintPlugin = require('sass-lint-webpack')
new VueLoaderPlugin(),
  module.exports = {
    watchOptions: {
      ignored: '/node_modules/**'
    },
    mode: 'development',
    // devtool: 'eval-source-map',
    // entry: [resolve(__dirname, '..', 'src_', 'js', 'index.js')],
    entry: [resolve(__dirname, 'src_', 'main.js')],
    output: {
      filename: 'bundle.js',
      path: resolve(__dirname, 'build')
    },
    resolve: {
      alias: {
        'vue$': 'vue/dist/vue.esm.js',
        '.@': resolve(__dirname, 'src_')
      },
      extensions: ['*', '.js', '.vue', '.json']
    },
    module: {
      rules: [
        {
          test: /\.vue$/,
          loader: 'vue-loader',
          options: {
            loaders: {}
          }
        },

        {
          test: /\.(png|jpe?g|gif)$/i,
          exclude: /node_modules/,
          use: [
            {
              loader: 'file-loader',
              options: {
                name: '[path][name].[ext]'
              }
            }
          ]
        },
        {
          test: /\.(html)$/,
          exclude: /node_modules/,
          use: {
            loader: 'html-loader',
            options: {
              attrs: ['img:src'],
              removeAttributeQuotes: false,
              minimize: false,
              removeComments: false,
              collapseWhitespace: false
            }
          }
        },
        {
          test: /\.(s[ac]ss)$/i,
          exclude: /node_modules/,
          use: [{
            loader: MiniCssExtractPlugin.loader
          },
            'css-loader',
          {
            loader: 'postcss-loader',
            options: {
              plugins: () => [require('autoprefixer')]
            }
          },
          {
            loader: 'sass-loader',
            options: {
              sourceMap: true
            }
          },
          ]
        },
        {
          test: /\.css$/,
          loader: "style-loader!css-loader"
        },
        {
          test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
          loader: 'url-loader'
        },
        {
          test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
          loader: 'file-loader'
        },
        {
          test: /\.(j|t)sx?$/,
          exclude: /node_modules/,
          use: [
            {
              loader: 'babel-loader',
              options: {
                babelrc: true
              }
            }, {
              loader: 'eslint-loader',
              options: {
                emitWarning: true,
                enforce: 'pre'
              }
            }]
        }
      ]
    },
    plugins: [
      new VueLoaderPlugin(),
      new CleanWebpackPlugin(),
      // new HtmlWebpackPlugin({
      //   template: resolve(__dirname, 'src_', 'index.html')
      // }),
      new MiniCssExtractPlugin({
        filename: 'bundle.css',
      }),
      new SassLintPlugin()
    ],


  }
